Search AutoCompletion
====================

A Rust implementation of Search Auto Completion.

Usage
-----

Create a new AutoComplete struct, you can use default(Empty) or new(Must be used with pre-defined words).

License
-------
The license is `GNU General Public License v3.0`
